import { defineConfig } from "vite";
import react from "@vitejs/plugin-react";
import ViteTsConfigPathsPlugin from "vite-tsconfig-paths";

export default defineConfig({
  server: {
    port: 3000,
    host: true,
    strictPort: true,
  },

  plugins: [
    react(),
    ViteTsConfigPathsPlugin({
      root: "",
    }),
  ],
});
