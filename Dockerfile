FROM node:18-bullseye-slim

RUN apt-get update && apt-get install -y git tini procps

WORKDIR /app

COPY src/ ./src/

COPY public/ ./public/

COPY package.json pnpm-lock.yaml ./

COPY tsconfig.json ./

COPY tsconfig.node.json ./

COPY vite.config.ts ./

COPY index.html ./

RUN npm -g i pnpm@7.13.2

RUN pnpm install

CMD ["npm", "run", "dev"]